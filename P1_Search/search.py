# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem: SearchProblem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print("Start:", problem.getStartState())
    print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
    print("Start's successors:", problem.getSuccessors(problem.getStartState()))
    """
    "*** YOUR CODE HERE ***"

    # Algoritmo: Búsqueda en grafo
    
    # Crea una pila vacía utilizando la clase Stack de la biblioteca util.
    stack = util.Stack()  
    # Obtiene el estado inicial del problema.
    startState = problem.getStartState()  
    #Inserta una tupla en la pila que contiene el estado inicial y una lista vacía que representará el camino.
    stack.push((startState, []))  
    # Crea un conjunto vacío para mantener un registro de los estados explorados.
    explored = set()  

    # Mientras la pila no esté vacía (quedan nodos por explorar)
    while not stack.isEmpty(): 
        # Saca el nodo más reciente de la pila. 
        currentState, solution = stack.pop()  

         # Si el estado actual es el estado objetivo devuelve el camino hasta este estado objetivo como solución.
        if problem.isGoalState(currentState): 
            return solution  

        """Si el estado actual no ha sido explorado antes marca el estado actual como explorado,
        obtiene los sucesores del estado actual y para cada sucesor."""
        
        if currentState not in explored:  
            explored.add(currentState) 
            successors = problem.getSuccessors(currentState)  

            """Para cada sucesor, si el sucesor no ha sido explorado antes, crea un nuevo camino agregando 
            la acción correspondiente"""
            for nextState, action, _ in successors:  
                if nextState not in explored: 
                    new_solution = solution + [action] 
                     # Inserta el sucesor y el nuevo camino en la pila para su exploración posterior.
                    stack.push((nextState, new_solution)) 
                    
    return None


def breadthFirstSearch(problem: SearchProblem):
    """Search the shallowest nodes in the search tree first.
    El código es similar al de la búsqueda en profundidad, pero en lugar de utilizar una pila,
    utiliza una cola. Esto hace que la búsqueda sea en anchura en lugar de en profundidad."""

    queue = util.Queue()
    startState = (problem.getStartState(), [])
    queue.push(startState)
    explored = []
    
    # Mientras la cola no esté vacía (quedan nodos por explorar)
    while not queue.isEmpty():
        node, path = queue.pop()
        # Si el estado actual es el estado objetivo devuelve el camino hasta este estado objetivo como solución.
        if problem.isGoalState(node):
            return path
        # Si el estado actual no ha sido explorado antes, marca el estado actual como explorado, obtiene los sucesores del estado actual y para cada sucesor.
        if not node in explored:
            explored.append(node)
            for new_solution, nextState, _ in problem.getSuccessors(node):
                queue.push((new_solution, path + [nextState])) 

    return None

def uniformCostSearch(problem: SearchProblem):
    """Search the node of least total cost first."""
    queue = util.PriorityQueue()  
    startState = problem.getStartState()  

    # Inserta una tupla en la pila que contiene el estado inicial y una lista vacía que representará el camino. Los otros 0 sirven para inicializar el coste total a 0.
    queue.push((startState, [], 0), 0)  
    explored = set()  

    while not queue.isEmpty(): 
        currentState, solution, _ = queue.pop()  

         # Si el estado actual es el estado objetivo devuelve el camino hasta este estado objetivo como solución.
        if problem.isGoalState(currentState): 
            return solution  

        """Si el estado actual no ha sido explorado antes marca el estado actual como explorado,
        obtiene los sucesores del estado actual y para cada sucesor."""
        
        if currentState not in explored:  
            explored.add(currentState)
            successors = problem.getSuccessors(currentState)  

            """Para cada sucesor, si el sucesor no ha sido explorado antes, crea un nuevo camino agregando 
            la acción correspondiente"""
            for nextState, action, _ in successors:  
                if nextState not in explored: 
                    new_solution = solution + [action] 

                    # Calcula el coste total del nuevo camino.
                    new_total_cost = problem.getCostOfActions(new_solution)
                     # Inserta el sucesor y el nuevo camino en la pila para su exploración posterior.
                    queue.push((nextState, new_solution, new_total_cost), new_total_cost) 

    return None

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem: SearchProblem, heuristic=nullHeuristic):
    queue = util.PriorityQueue()
    # Crea un diccionario para almacenar los costes de los nodos.
    counts = util.Counter()
    # Obtiene el estado inicial del problema.
    currentState = (problem.getStartState(), []) 
    # Inserta una tupla en la pila que contiene el estado inicial y una lista vacía que representará el camino.   
    counts[str(currentState[0])] += heuristic(currentState[0], problem)
    # Inserta el estado inicial y el coste total en la cola de prioridad.
    queue.push(currentState, counts[str(currentState[0])])
    # Crea una lista vacía para almacenar los nodos explorados.
    explored = []

    # Mientras la cola no esté vacía (quedan nodos por explorar)
    while not queue.isEmpty():
        # Saca el nodo más reciente de la cola de prioridad.
        node, path = queue.pop()

        if problem.isGoalState(node):
            return path
        # Si el estado actual no ha sido explorado antes, marca el estado actual como explorado, 
        # obtiene los sucesores del estado actual y para cada sucesor.

        if not node in explored:
            explored.append(node)
            # Para cada sucesor, si el sucesor no ha sido explorado antes, crea un nuevo camino agregando
            
            for coord, move, _ in problem.getSuccessors(node):
                newpath = path + [move]
                # Calcula el coste total del nuevo camino.
                counts[str(coord)] = problem.getCostOfActions(newpath)
                # Inserta el sucesor y el nuevo camino en la cola de prioridad para su exploración posterior.
                counts[str(coord)] += heuristic(coord, problem)
                queue.push((coord, newpath), counts[str(coord)]) 


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch